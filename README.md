Description
===========

A very basic roguelike built from NomadHermit's instructions found [here](https://nomadhermitsgrove.prometheaninteractive.com/category/roguelike/).

Developped live on Twitch, check out my [channel](https://www.twitch.tv/guiguisurlinux) if you speak French.

I used Kenney's Roguelike/RPG pack found [here](https://kenney.nl/assets/roguelike-rpg-pack). 

Playable demo [here](https://guiguisurlinux.itch.io/roguelike-tutorial-demo) (if you like games with no win condition).

Differences with NomadHermit's tutorial
=======================================

I changed the code in a few ways, either to simplify or complete the project:
* The combat and follow logic is "complete".
* Enemies don't sleep.
* Enemies don't destroy their object on death.
* I changed Enemy Data to differentiate types and instances.
* DungeonManager is its own GameObject and not assumed to be a part of GameManager.
* I removed all camera code in favor of Cinemachine (set x/y/z damping to 0 to avoid tearing).
* Pressing the Escape key causes the application to end.
* I messed up the indentation somewhat.
* Other misc changes, I didn't really think to write them down until now, sorry!
